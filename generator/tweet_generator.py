import random
from argparse import ArgumentParser
from kafka import KafkaProducer
import json

parser = ArgumentParser(prog="simple_kafka_producer",
                        description="Simple Kafka producer which reads data from file and pushes it on the topic")

parser.add_argument("-b", "--bootstrap_servers", nargs='*', default=["localhost:9092"],
                    help="List of Kafka bootstrap servers.")
parser.add_argument("-n", "--num_tweets", default=10000, help="Number of tweets to generate.")
parser.add_argument("-r", "--tweets_id_range", default=1000000, help="Tweets id range.")

args = parser.parse_args()

num_tweets = args.num_tweets

tweets_id_range = args.tweets_id_range
if num_tweets > tweets_id_range:
    tweets_id_range = num_tweets + 1

tweet_ids = random.sample(range(tweets_id_range), num_tweets)
customers = ['google', 'facebook', 'twitter', 'apple', 'amazon']

producer = KafkaProducer(bootstrap_servers=args.bootstrap_servers, value_serializer=lambda x: json.dumps(x).encode('utf-8'))


for tweet_id in tweet_ids:
    if tweet_id % 3 == 0 or tweet_id % 5 == 0:
        message = {'tweetId': tweet_id, 'customer': {'name': random.choice(customers)}}
        producer.send('tweet_subscriptions', value=message)
        print(json.dumps(message))
    message = {'tweetId': tweet_id}
    producer.send('tweet_impressions', value=message)
    print(json.dumps(message))
