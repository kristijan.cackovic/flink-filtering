package hr.fer.ztel.rassus.kcackovic.kafka;

import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;

import java.util.List;
import java.util.Properties;

public class KafkaConsumerFactory {

    private static KafkaConsumerFactory INSTANCE = new KafkaConsumerFactory();

    private KafkaConsumerFactory() {
    }

    public static KafkaConsumerFactory getInstance() {
        return INSTANCE;
    }

    public <T> FlinkKafkaConsumer<T> createDataStream(List<String> topics, DeserializationSchema<T> schema, Properties properties) {
        return new FlinkKafkaConsumer<>(topics, schema, properties);
    }
}
