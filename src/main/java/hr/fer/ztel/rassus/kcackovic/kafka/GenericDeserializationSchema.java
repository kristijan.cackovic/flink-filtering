package hr.fer.ztel.rassus.kcackovic.kafka;

import com.google.gson.Gson;
import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.api.common.typeinfo.TypeInformation;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class GenericDeserializationSchema<T> implements DeserializationSchema<T> {

    private final Class<T> tClass;
    private static final Gson parser = new Gson();

    public GenericDeserializationSchema(Class<T> tClass) {
        this.tClass = tClass;
    }

    @Override
    public T deserialize(byte[] bytes) throws IOException {
        return parser.fromJson(new String(bytes, StandardCharsets.UTF_8), tClass);
    }

    @Override
    public boolean isEndOfStream(T t) {
        return false;
    }

    @Override
    public TypeInformation<T> getProducedType() {
        return TypeInformation.of(tClass);
    }
}
