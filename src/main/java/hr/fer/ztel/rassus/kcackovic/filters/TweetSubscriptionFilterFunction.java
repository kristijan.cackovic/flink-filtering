package hr.fer.ztel.rassus.kcackovic.filters;

import hr.fer.ztel.rassus.kcackovic.domain.Customer;
import hr.fer.ztel.rassus.kcackovic.domain.CustomerImpression;
import hr.fer.ztel.rassus.kcackovic.domain.TweetImpression;
import hr.fer.ztel.rassus.kcackovic.domain.TweetSubscription;
import org.apache.flink.api.common.state.ListState;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.streaming.api.functions.co.RichCoFlatMapFunction;
import org.apache.flink.util.Collector;

/**
 * This function consumes a connected stream.  The two individual streams are a stream of TweetImpressions
 * and a stream of TweetSubscriptions.  The TweetSubscription indicates that a customer would
 * like to receive TweetImpressions for the given tweet.  For each TweetImpression consumed this function
 * emits a message for *each* customer that has subscribed to that tweet.
 */
public class TweetSubscriptionFilterFunction extends RichCoFlatMapFunction<TweetImpression, TweetSubscription, CustomerImpression> {

    private ListStateDescriptor<Customer> tweetSubscriptionsStateDesc =
            new ListStateDescriptor<>("tweetSubscriptions", Customer.class);

    @Override
    public void flatMap1(TweetImpression impression, Collector<CustomerImpression> out) throws Exception {
        Iterable<Customer> customers = getRuntimeContext().getListState(tweetSubscriptionsStateDesc).get();
        if (customers != null) {
            for (Customer customer : customers) {
                out.collect(new CustomerImpression(customer, impression));
            }
        }
    }

    @Override
    public void flatMap2(TweetSubscription subscription, Collector<CustomerImpression> out) throws Exception {
        ListState<Customer> subscriptionState = getRuntimeContext().getListState(tweetSubscriptionsStateDesc);

        boolean customerPresent = false;
        for (Customer c : subscriptionState.get()) {
            if (c.equals(subscription.getCustomer())) {
                customerPresent = true;
            }
        }

        if (!customerPresent) {
            subscriptionState.add(subscription.getCustomer());
            System.err.println(String.format("Sub-task %d: Enabling delivery of impression for Tweet(%d) to %s",
                    getRuntimeContext().getIndexOfThisSubtask() + 1,
                    subscription.getTweetId(),
                    subscription.getCustomer()));
        }
    }
}
