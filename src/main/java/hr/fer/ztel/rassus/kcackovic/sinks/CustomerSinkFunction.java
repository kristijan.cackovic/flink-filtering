package hr.fer.ztel.rassus.kcackovic.sinks;

import hr.fer.ztel.rassus.kcackovic.domain.Customer;
import hr.fer.ztel.rassus.kcackovic.domain.CustomerImpression;
import org.apache.flink.streaming.api.functions.sink.RichSinkFunction;

/**
 * This sink is for messages to be delivered to one particular customer.
 */
public class CustomerSinkFunction extends RichSinkFunction<CustomerImpression> {
    private final Customer customer;

    public CustomerSinkFunction(Customer customer) {
        this.customer = customer;
    }

    @Override
    public void invoke(CustomerImpression impression) throws Exception {
        System.out.println(String.format("Delivering %s to sink for %s ", impression.getTweetImpression(), customer));
    }
}
