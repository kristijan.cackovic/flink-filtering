package hr.fer.ztel.rassus.kcackovic;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import hr.fer.ztel.rassus.kcackovic.domain.Customer;
import hr.fer.ztel.rassus.kcackovic.domain.CustomerImpression;
import hr.fer.ztel.rassus.kcackovic.domain.TweetImpression;
import hr.fer.ztel.rassus.kcackovic.domain.TweetSubscription;
import hr.fer.ztel.rassus.kcackovic.filters.TweetSubscriptionFilterFunction;
import hr.fer.ztel.rassus.kcackovic.kafka.GenericDeserializationSchema;
import hr.fer.ztel.rassus.kcackovic.kafka.KafkaConsumerFactory;
import hr.fer.ztel.rassus.kcackovic.sinks.CustomerSinkFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.restartstrategy.RestartStrategies;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.collector.selector.OutputSelector;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSink;
import org.apache.flink.streaming.api.datastream.SplitStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;


public class TweetImpressionFilteringJob {

    private static long DEDUPE_CACHE_EXPIRATION_TIME_MS = 1_000;

    private static Gson GSON = new Gson();

    public static void main(String[] args) throws Exception {

        // Parse command line parameters
        ParameterTool parameterTool = ParameterTool.fromArgs(args);

        StreamExecutionEnvironment env;
        // Setup the execution environment
        if (parameterTool.getBoolean("test", false)) {
            env = StreamExecutionEnvironment.createLocalEnvironment(1);
        } else {
            env = StreamExecutionEnvironment.getExecutionEnvironment();
            env.setParallelism(parameterTool.getInt("parallelism", 1));
        }

        env.enableCheckpointing(1000);
        env.setRestartStrategy(RestartStrategies.fixedDelayRestart(Integer.MAX_VALUE, 1000));

        String bootstrapServers = parameterTool
                .getRequired("bootstrap_servers");
        String zookeeper = parameterTool.getRequired("zookeeper_connect");

        final Properties kafkaProperties = new Properties();
        kafkaProperties.put("bootstrap.servers", bootstrapServers);
        kafkaProperties.put("group.id", TweetImpressionFilteringJob.class.getName());
        kafkaProperties.put("zookeeper.connect", zookeeper);

        // Stream of updates to subscriptions, partitioned by tweetId, read from socket
        DataStream<TweetSubscription> filterUpdateStream = env.addSource(
                KafkaConsumerFactory
                        .getInstance()
                        .createDataStream(Arrays.asList("tweet_subscriptions"), new GenericDeserializationSchema<>(TweetSubscription.class), kafkaProperties))
                .setParallelism(1)
                .keyBy(TweetSubscription.getKeySelector());

        // TweetImpression stream, partitioned by tweetId
        DataStream<TweetImpression> tweetStream = env.addSource(
                KafkaConsumerFactory
                        .getInstance()
                        .createDataStream(Arrays.asList("tweet_impressions"), new GenericDeserializationSchema<>(TweetImpression.class), kafkaProperties)
                , "TweetImpression Source")
                .setParallelism(1)
                .keyBy(TweetImpression.getKeySelector());

        // Run the tweet impressions past the filters and emit those that customers have requested
        DataStream<CustomerImpression> filteredStream = tweetStream
                .connect(filterUpdateStream)
                .flatMap(new TweetSubscriptionFilterFunction());

        // Create a seperate sink for each customer
        setupCustomerSinks(filteredStream);

        // Run it
        env.execute(TweetImpressionFilteringJob.class.getName());
    }

    private static MapFunction<String, TweetSubscription> stringToTweetSubscription() {
        return new MapFunction<String, TweetSubscription>() {
            @Override
            public TweetSubscription map(String value) throws Exception {
                return GSON.fromJson(value, TweetSubscription.class);
            }
        };
    }

    private static String customerStreamName(Customer customer) {
        return String.format("customer-%s", customer.getName());
    }

    private static OutputSelector<CustomerImpression> customerOutputSelector() {
        return new OutputSelector<CustomerImpression>() {
            @Override
            public Iterable<String> select(CustomerImpression msg) {
                return Lists.newArrayList(customerStreamName(msg.getCustomer()));
            }
        };
    }

    private static DataStreamSink<CustomerImpression>[] setupCustomerSinks(DataStream<CustomerImpression> msgStream) {
        // Split the stream into multiple streams by customer Id
        SplitStream<CustomerImpression> splitStream = msgStream.split(customerOutputSelector());

        // Tie a separate sink to each fork of the split stream
        List<Customer> customers = Customer.getAllCustomers();
        DataStreamSink<CustomerImpression>[] customerSinks = new DataStreamSink[customers.size()];

        int i = 0;
        for (Customer customer : customers) {
            customerSinks[i++] = splitStream
                    .select(customerStreamName(customer))
                    .addSink(new CustomerSinkFunction(customer));
        }
        return customerSinks;
    }

}
